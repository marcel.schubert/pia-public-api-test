import pytest
import requests
import datetime
import json
from selenium import webdriver
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Configurations
RELEASE_URL = "https://pia-release.de"
BASE_URL = f"{RELEASE_URL}/public/api/v1"
TOKEN_URL = "https://pia-release.de/api/v1/auth/realms/pia-admin-realm/protocol/openid-connect/token"
CLIENT_ID = 'pia-public-api-api-test'
CLIENT_SECRET = 'cgsmf6C7YflD90XIIYAOF4D8k0ocOEas'
STUDY_NAME = "API-Test"

# Endpoints
def url_questionnaire_instances(pseudonym, identifier=None, answers=False):
    if identifier and answers:
        return f"{BASE_URL}/studies/{STUDY_NAME}/participants/{pseudonym}/questionnaire-instances/{identifier}/answers"
    elif identifier:
        return f"{BASE_URL}/studies/{STUDY_NAME}/participants/{pseudonym}/questionnaire-instances/{identifier}"
    else:
        return f"{BASE_URL}/studies/{STUDY_NAME}/participants/{pseudonym}/questionnaire-instances"

def url_answers(pseudonym, identifier):
    return url_questionnaire_instances(pseudonym, identifier, answers=True)

def url_participants(pseudonym=None):
    if pseudonym:
        return f"{BASE_URL}/studies/{STUDY_NAME}/participants/{pseudonym}"
    else:
        return f"{BASE_URL}/studies/{STUDY_NAME}/participants"
 


@pytest.fixture(scope="session")
def driver():
    firefox_options = FirefoxOptions()
    firefox_options.add_argument("--headless")
    driver = webdriver.Firefox(options = firefox_options)
    driver.get(RELEASE_URL)
    yield driver
    driver.quit()
    
@pytest.fixture(autouse=True)
def reset_driver_url(driver):
    # Reset the URL of the driver after each test or fixture
    driver.get(RELEASE_URL)

@pytest.fixture(scope="function")
def pseudonym():
    letters = 'pia'
    end_time = datetime.datetime.now(tz=datetime.timezone.utc)
    start_time = datetime.datetime(1970, 1, 1, tzinfo=datetime.timezone.utc)
    time_in_millis = int(((end_time-start_time)).total_seconds()*1000)
    pseudonym = f'{letters}-{time_in_millis}'
    return pseudonym

# Fixtures für Authentifizierung und API-Zugriff
@pytest.fixture(scope="module")
def auth_token():
    payload = {
    'grant_type': 'client_credentials',
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET}
    response = requests.post(TOKEN_URL, data=payload)
    assert response.status_code == 200
    token_data = response.json()
    return token_data.get('access_token')

@pytest.fixture(scope="module")
def header(auth_token):
    return {
    'Authorization': f'Bearer {auth_token}',
    'Content-Type': 'application/json',
    'Accept': 'application/json'
    }

@pytest.fixture(scope="function")
def create_and_cleanup_participant(pseudonym, header, driver):
    assert header != None
    assert len(pseudonym) > 1

    #create participant
    #url = f"{BASE_URL}/studies/{STUDY_NAME}/participants"
    data = {
        "pseudonym": pseudonym,
        "ids": f"ids-{pseudonym}",
        "studyCenter": "Hannover",
        "examinationWave": 0,
        "isTestParticipant": False,
    }
    response = requests.post(url=url_participants(), json=data, headers=header)
    assert response.status_code == 201
    
    data = json.loads(response.content.decode("utf8"))
    old_password = data["password"]
    new_password = "neuesPasswort123!"
    login_user_and_set_new_password(pseudonym,old_password,new_password, driver)
    
    #call other testcases
    yield header, pseudonym

    # delete participant
    #delete_url = f"{BASE_URL}/studies/{STUDY_NAME}/participants/{pseudonym}"
    params = {
        "deletionType": "full"
    }
    response = requests.delete(url = url_participants(pseudonym), headers=header, params=params)
    assert response.status_code in [204, 404]  # accept 'No Content' and 'Not Found'

def login_user_and_set_new_password(username, old_password, new_password, driver):
    try:
        #Login on first page
        username_input = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.ID, "username"))
        )
        password_input = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.ID, "password"))
        )
        login_button = driver.find_element(By.ID, "kc-login")
        
        username_input.send_keys(username)
        password_input.send_keys(old_password)
        login_button.click()
        
        #set new password
        password_new_input = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.ID, "password-new"))
        )
        password_confirm_input = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.ID, "password-confirm"))
        )
        set_password_button = driver.find_element(By.ID, "kc-form-buttons")
        
        password_new_input.send_keys(new_password)
        password_confirm_input.send_keys(new_password)
        set_password_button.click()
        
        #logout
        logout = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, "mat-button-toggle-1"))
        )
        logout.click()
        
        #confirm logout
        confirm_logout = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, "confirmButton"))
        )
        confirm_logout.click()
        
        #wait till logged out
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.ID, "username"))
        )
    except Exception as e:
        print(f"An error occurred: {e}")