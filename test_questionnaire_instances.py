import requests
import conftest
import pytest
import json
from conftest import url_questionnaire_instances
from conftest import url_answers

@pytest.mark.get
def test_get_questionnaire_instance(create_and_cleanup_participant):
    header, pseudonym =  create_and_cleanup_participant
    response = requests.get(url_questionnaire_instances(pseudonym), headers=header)
    assert response.status_code == 200
    
@pytest.mark.patch
def test_patch_questionnaire_instance_id(create_and_cleanup_participant):
    header, pseudonym = create_and_cleanup_participant
    response = requests.get(url_questionnaire_instances(pseudonym), headers=header)
    questionnaire_data = json.loads(response.content.decode('utf8'))
    
    assert len(questionnaire_data) > 0
    
    questionnaire_id = questionnaire_data[0]['id']
    
    patch_data =  {
                "status": "in_progress"
            }
    response = requests.patch(url_questionnaire_instances(pseudonym,questionnaire_id), json=patch_data, headers = header)  
    assert response.status_code == 200
    
@pytest.mark.patch 
def test_patch_questionnaire_instance_custom_name(create_and_cleanup_participant):
    header, pseudonym = create_and_cleanup_participant
    response = requests.get(url_questionnaire_instances(pseudonym), headers=header)
    questionnaire_data = json.loads(response.content.decode('utf8'))
    
    assert len(questionnaire_data) > 0
    
    questionnaire_custom_name = questionnaire_data[0]['questionnaireCustomName']
    
    patch_data =  {
                "status": "in_progress"
            }
    response = requests.patch(url_questionnaire_instances(pseudonym,questionnaire_custom_name), json=patch_data, headers = header)  
    assert response.status_code == 200

@pytest.mark.post
def test_post_questionnaire_instance(create_and_cleanup_participant):
    header, pseudonym = create_and_cleanup_participant
    response = requests.get(url_questionnaire_instances(pseudonym), headers=header)
    questionnaire_data = json.loads(response.content.decode('utf8'))
    
    assert len(questionnaire_data) > 0
    
    questionnaire_id = questionnaire_data[0]['id']
    #contains textvalue, multiple select value, single select value AND an internal condition
    post_data = [{"questionVariableName": "annotation_question",
                  "answerOptionVariableName": "annotation_answer",
                  "value": "value",},
                 {"questionVariableName": "apitest_weather_location_question",
                  "answerOptionVariableName": "apitest_weather_location_answer",
                  "value": [0,2]},
                 {"questionVariableName": "apitest_condition_rain_question",
                  "answerOptionVariableName": "apitest_condition_rain_answer",
                  "value": 1},
                ]
    response = requests.post(url_answers(pseudonym,questionnaire_id), json=post_data, headers = header)
    assert response.status_code == 200