import requests
import json

#modify
RELEASE_URL = "https://pia-release.de"
BASE_URL = f"{RELEASE_URL}/public/api/v1"
TOKEN_URL = f"{RELEASE_URL}/api/v1/auth/realms/pia-admin-realm/protocol/openid-connect/token"
CLIENT_ID = 'pia-public-api-nako-teststudie'
CLIENT_SECRET_RELEASE = 'client_secret_here'
STUDY_NAME = "NAKO-Teststudie"

def auth_token():
    payload = {
    'grant_type': 'client_credentials',
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET_RELEASE}
    response = requests.post(TOKEN_URL, data=payload)
    assert response.status_code == 200
    token_data = response.json()
    return token_data.get('access_token')

def header(auth_token):
    return {
    'Authorization': f'Bearer {auth_token}',
    'Content-Type': 'application/json',
    'Accept': 'application/json'
    }

PSEUDONYM = "nako-1212121244"
participant_data = {
        "pseudonym": f"{PSEUDONYM}",
        "ids": f"ids-{PSEUDONYM}",
        "studyCenter": "Testcenter",
        "examinationWave": 0,
        "isTestParticipant": True,
    }

#create participant
response = requests.post(url=f"{BASE_URL}/studies/{STUDY_NAME}/participants", json=participant_data, headers=header(auth_token()))
assert response.status_code == 201
participant_data = json.loads(response.content.decode('utf8'))
print(json.dumps(participant_data, indent=4, sort_keys=True))

#change to test participant
response = requests.patch(url=f"{BASE_URL}/studies/{STUDY_NAME}/participants/{PSEUDONYM}", json = {"isTestParticipant": True,}, headers=header(auth_token()))
assert response.status_code == 200

#get questionnaire instances of participant
response = requests.get(f"{BASE_URL}/studies/{STUDY_NAME}/participants/{PSEUDONYM}/questionnaire-instances", headers=header(auth_token()))
participant_data = json.loads(response.content.decode('utf8'))
print(json.dumps(participant_data, indent=4, sort_keys=True))

#search for the questionnaire to be changed
questionnaire_id = 134
questionnaire_instance_id = next(
    (questionnaire["id"] for questionnaire in participant_data if questionnaire["questionnaireId"] == questionnaire_id), 
    -1)

assert questionnaire_instance_id != -1

#Answer investigation team questionnaire to unlock the conditional questionnaire
questionnaire_answer_data = [{
    "questionVariableName": "pia_IADL_3u_show",
    "answerOptionVariableName": "pia_IADL_3u_show",
    "value": 1,
    }]
response = requests.post(url=f"{BASE_URL}/studies/{STUDY_NAME}/participants/{PSEUDONYM}/questionnaire-instances/{questionnaire_instance_id}/answers", json=questionnaire_answer_data, headers=header(auth_token()))
assert response.status_code == 200

questionnaire_status = {
    "status": "released",
    }
response = requests.patch(url=f"{BASE_URL}/studies/{STUDY_NAME}/participants/{PSEUDONYM}/questionnaire-instances/{questionnaire_instance_id}", json=questionnaire_status, headers=header(auth_token()))
assert response.status_code == 200

#get questionnaire instances of participant
response = requests.get(f"{BASE_URL}/studies/{STUDY_NAME}/participants/{PSEUDONYM}/questionnaire-instances", headers=header(auth_token()))
participant_data = json.loads(response.content.decode('utf8'))
print(json.dumps(participant_data, indent=4, sort_keys=True)) # shows questionnaire "Alltagsaktivitäten bedingt" -> the condition/workflow worked

#delete participant
#response = requests.delete(url=f"{BASE_URL}/studies/{STUDY_NAME}/participants/{PSEUDONYM}", headers=header(auth_token()))
#assert response.status_code == 204






