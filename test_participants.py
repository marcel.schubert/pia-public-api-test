import requests
import conftest
import pytest
import json
from conftest import url_participants
    
@pytest.mark.get
def test_get_all_participants(create_and_cleanup_participant):
    header, pseudonym = create_and_cleanup_participant
    response = requests.get(url_participants(), headers=header)
    assert response.status_code == 200
    participants_data = json.loads(response.content.decode('utf8'))
    assert participants_data[0]['pseudonym'] == pseudonym
    
@pytest.mark.get
def test_get_specific_participant(create_and_cleanup_participant):
    header, pseudonym = create_and_cleanup_participant
    response = requests.get(url_participants(pseudonym),headers = header)
    assert response.status_code == 200
    specific_participant_data = json.loads(response.content.decode('utf8'))
    assert specific_participant_data['pseudonym'] == pseudonym

@pytest.mark.patch
def test_patch_participant(create_and_cleanup_participant):
    header, pseudonym = create_and_cleanup_participant
    
    #update fields
    ids = "test_IDS"
    study_center = "Celle"
    examination_wave = 42
    is_test_participant = True
    
    patch_data = {
                    "ids": ids,
                    "studyCenter": study_center,
                    "examinationWave": examination_wave,
                    "isTestParticipant": is_test_participant,
                 }
    response_patch = requests.patch(url=url_participants(pseudonym),json=patch_data, headers=header)
    assert response_patch.status_code == 200
    
    #check if update was successful
    response_get = requests.get(url = url_participants(pseudonym), headers=header)
    specific_participant_data = json.loads(response_get.content.decode('utf8'))

    assert specific_participant_data['ids'] == ids
    assert specific_participant_data['studyCenter'] == study_center
    assert specific_participant_data['examinationWave'] == examination_wave
    assert specific_participant_data['isTestParticipant'] == is_test_participant

@pytest.mark.delete
def test_delete_participant(create_and_cleanup_participant):
    header, pseudonym = create_and_cleanup_participant
    
    response_delete = requests.delete(url = url_participants(pseudonym), headers= header)
    assert response_delete.status_code == 204
    
    response_get = requests.get(url = url_participants(pseudonym), headers=header)
    assert response_get.status_code == 200
    
    deleted_participant_data = json.loads(response_get.content.decode('utf8'))
    assert deleted_participant_data['status'] == "deleted"
    